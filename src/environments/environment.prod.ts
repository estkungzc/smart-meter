/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyCz1dvoMt5iElNAYXKQaU7BIfJubtiyfb8",
    authDomain: "homeless-uranium.firebaseapp.com",
    databaseURL: "https://homeless-uranium.firebaseio.com",
    projectId: "homeless-uranium",
    storageBucket: "homeless-uranium.appspot.com",
    messagingSenderId: "917182459407",
  }
};
