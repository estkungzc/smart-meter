import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SourceDataComponent } from './source-data.component';

const routes: Routes = [{
  path: '',
  component: SourceDataComponent
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SourceDataRoutingModule { }

export const routedComponents = [
  SourceDataComponent
];
