import { Component, OnInit, OnDestroy } from '@angular/core';
import { LocalDataSource } from 'ng2-smart-table';
import { MeterService } from '../../@core/services';
import * as moment from 'moment';
import { DatePipe } from '@angular/common';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
moment.locale("th");

@Component({
  selector: 'source-data',
  templateUrl: './source-data.component.html',
  styleUrls: ['./source-data.component.scss'],
})
export class SourceDataComponent implements OnInit, OnDestroy {

  private unsubscribe$: Subject<boolean> = new Subject();

  settings = {}

  source: LocalDataSource = new LocalDataSource();


  constructor(private meterService: MeterService) {
    this.settings = {
      mode: 'external',
      actions: false,
      hideSubHeader: true,
      columns: {
        index:{
          title: '#',
          type: 'text',
          valuePrepareFunction: (value,row,cell) => {
            const pager = this.source.getPaging();
            const ret = (pager.page-1) * pager.perPage + cell.row.index+1;
            return ret;
           }
        },
        time: {
          title: 'Time',
          type: 'string',
          valuePrepareFunction: (value) => {
            if (!value) return '';
            return moment.unix(value).format('MMMM Do YYYY, h:mm:ss');
          },
        },
        activePower: {
          title: 'Active Power(Watts)',
          type: 'number',
        },
        apparentPower: {
          title: 'Apparent Power(VoltAmps)',
          type: 'number',
        },
        current: {
          title: 'Current(Amps)',
          type: 'number',
        },
        exportActiveEnergy: {
          title: 'Export Active Energy(kWh)',
          type: 'number',
        },
        exportReactiveEnergy: {
          title: 'Export Reactive Energy(kVArh)',
          type: 'number',
        },
        frequency: {
          title: 'Frequency(Hz)',
          type: 'number',
        },
        importActiveEnergy: {
          title: 'Import Active Energy(kWh)',
          type: 'number',
        },
        importReactiveEnergy: {
          title: 'Import Reactive Energy(kVArh)',
          type: 'number',
        },
        phaseAngle: {
          title: 'Phase Angle(Degrees)',
          type: 'number',
        },
        powerFactor: {
          title: 'Power Factor',
          type: 'number',
        },
        reactivePower: {
          title: 'Reactive Power(VAr)',
          type: 'number',
        },
        totalActiveEnergy: {
          title: 'Total Active Energy(kWh)',
          type: 'number',
        },
        totalReactiveEnergy: {
          title: 'Total Reactive Energy(kVArh)',
          type: 'number',
        },
        voltage: {
          title: 'Voltage(Volts)',
          type: 'number',
        }
      },
    };

    this.meterService.getListData().pipe(takeUntil(this.unsubscribe$)).subscribe(data => {
      this.source.load(data.sort((a, b) => b.time - a.time))
    });
  }
  ngOnInit() {
  }
  ngOnDestroy() {
    this.unsubscribe$.next(true);
    this.unsubscribe$.complete();
  }
}
