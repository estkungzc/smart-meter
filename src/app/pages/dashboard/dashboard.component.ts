import { Component, OnDestroy, OnInit } from "@angular/core";
import { Subject } from "rxjs";
import { MeterService } from "../../@core/services";
import { switchMap, takeUntil } from "rxjs/operators";
import { NbThemeService, NbColorHelper } from "@nebular/theme";
import { ElectricityChart } from "../../@core/data/electricity";
import Chart from "chart.js";

import { LocalDataSource } from 'ng2-smart-table';

import * as moment from "moment";

moment.locale("th");

@Component({
  selector: "ngx-dashboard",
  styleUrls: ["./dashboard.component.scss"],
  templateUrl: "./dashboard.component.html"
})
export class DashboardComponent implements OnInit, OnDestroy {
  objectKeys = Object.keys;
  private unsubscribe$: Subject<boolean> = new Subject();

  themeSubscription: any;
  field = {};

  lineColors = [
    "primaryLight",
    "successLight",
    "infoLight",
    "warningLight",
    "dangerLight"
  ];

  public canvasCurrent: any;
  public ctxCurrent;
  public currentChart;

  public canvasVoltage: any;
  public ctxVoltage;
  public voltageChart;

  public canvasTotalActiveEnergy: any;
  public ctxTotalActiveEnergy;
  public totalActiveEnergyChart;

  public canvasActivePower: any;
  public ctxActivePower;
  public activePowerChart;

  public canvasPowerFactor: any;
  public ctxPowerFactor;
  public powerFactorChart;

  constructor(
    private meterService: MeterService,
    private theme: NbThemeService
  ) {}

  ngOnInit() {
    this.themeSubscription = this.theme.getJsTheme().subscribe(config => {
      const colors: any = config.variables;
      const chartjs: any = config.variables.chartjs;
      this.meterService
        .getChartData()
        .pipe(takeUntil(this.unsubscribe$))
        .subscribe(d => {
          this.canvasCurrent = document.getElementById("currentChart");
          this.ctxCurrent = this.canvasCurrent.getContext("2d");
          this.currentChart = new Chart(this.ctxCurrent, this.setChart(d, 'current', colors, chartjs));
          this.currentChart.update();

          this.canvasVoltage = document.getElementById("voltageChart");
          this.ctxVoltage = this.canvasVoltage.getContext("2d");
          this.voltageChart = new Chart(this.ctxVoltage, this.setChart(d, 'voltage', colors, chartjs));
          this.voltageChart.update();

          this.canvasTotalActiveEnergy = document.getElementById("totalActiveEnergyChart");
          this.ctxTotalActiveEnergy = this.canvasTotalActiveEnergy.getContext("2d");
          this.totalActiveEnergyChart = new Chart(this.ctxTotalActiveEnergy, this.setChart(d, 'totalActiveEnergy', colors, chartjs));
          this.totalActiveEnergyChart.update();

          this.canvasActivePower = document.getElementById("activePowerChart");
          this.ctxActivePower = this.canvasActivePower.getContext("2d");
          this.activePowerChart = new Chart(this.ctxActivePower, this.setChart(d, 'activePower', colors, chartjs));
          this.activePowerChart.update();

          this.canvasPowerFactor = document.getElementById("powerFactorChart");
          this.ctxPowerFactor = this.canvasPowerFactor.getContext("2d");
          this.powerFactorChart = new Chart(this.ctxPowerFactor, this.setChart(d, 'powerFactor', colors, chartjs));
          this.powerFactorChart.update();
        });
    });
  }

  setChart(data: ElectricityChart, key: string, colors, chartjs) {
    return {
      type: 'line',
      data: this.setChartData(data, key, colors[this.randomColor()]),
      options: this.setChartOption(data, key, chartjs)
    }
  }

  setChartData(data: ElectricityChart, key: string, colors) {
    const obj = data[key];
    return {
      labels: obj.time,
      datasets: [
        {
          data: obj.value,
          label: obj.title,
          borderColor: colors,
          lineTension: 0,
        }
      ]
    };
  }

  isEmptyObject(obj) {
    return (obj && (Object.keys(obj).length === 0));
  }

  setChartOption(data: ElectricityChart, key: string, chartjs) {
    return {
      responsive: true,
      maintainAspectRatio: false,
      scales: {
        xAxes: [
          {
            type: 'time',
            time: {
              unit: 'minute',
              displayFormats: {
                minute: 'h:mm a'
              }
          },
          distribution: 'series'
          ,
            display: true,
            scaleLabel: {
              display: true,
              labelString: "Time"
            },
            gridLines: {
              display: true,
              color: chartjs.axisLineColor
            },
            ticks: {
              fontColor: chartjs.textColor
            }
          }
        ],
        yAxes: [
          {
            display: true,
            scaleLabel: {
              display: true,
              labelString: data[key].unit
            },
            gridLines: {
              display: true,
              color: chartjs.axisLineColor
            },
            ticks: {
              fontColor: chartjs.textColor
            }
          }
        ]
      },
      legend: {
        labels: {
          fontColor: chartjs.textColor
        }
      }
    };
  }

  randomColor() {
    return this.lineColors[Math.floor(Math.random() * this.lineColors.length)];
 }

  ngOnDestroy() {
    this.unsubscribe$.next(true);
    this.unsubscribe$.complete();
  }


}
