import { NgModule } from '@angular/core';

import { ThemeModule } from '../../@theme/theme.module';
import { DashboardComponent } from './dashboard.component';
import { FormsModule } from '@angular/forms';

import { ChartModule } from 'angular2-chartjs';
import { NbCardModule } from '@nebular/theme';

@NgModule({
  imports: [
    FormsModule,
    ThemeModule,
    ChartModule,
    NbCardModule
  ],
  declarations: [
    DashboardComponent
  ],
})
export class DashboardModule { }
