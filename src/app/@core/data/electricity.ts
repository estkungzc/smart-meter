import { Observable } from 'rxjs';

export interface RawElectricity {
  'Active Power(Watts)': string;
  'Apparent Power(VoltAmps)': string;
  'Current(Amps)': string;
  'Export Active Energy(kWh)': string;
  'Export Reactive Energy(kVArh)': string;
  'Frequency(Hz)': string;
  'Import Active Energy(kWh)': string;
  'Import Reactive Energy(kVArh)': string;
  'Phase Angle(Degrees)': string;
  'Power Factor': string;
  'Reactive Power(VAr)': string;
  'Total Active Energy(kWh)': string;
  'Total Reactive Energy(kVArh)': string;
  'Voltage(Volts)': string;
  'time': number;
}

export interface Electricity {
  activePower: number;
  apparentPower: number;
  current: number;
  exportActiveEnergy: number;
  exportReactiveEnergy: number;
  frequency: number;
  importActiveEnergy: number;
  importReactiveEnergy: number;
  phaseAngle: number;
  powerFactor: number;
  reactivePower: number;
  totalActiveEnergy: number;
  totalReactiveEnergy: number;
  voltage: number;
  time: any;
}


export interface ElectricityChart {
  [field: string]: {
    title: string,
    unit: string,
    time: any[],
    value: number[]
  }
}



export abstract class ElectricityData {
  abstract getListData(): Observable<Electricity[]>;
  abstract getChartData(): Observable<ElectricityChart>;
}

export const MAP_DATA = {
  'Active Power(Watts)': 'activePower',
  'Apparent Power(VoltAmps)': 'apparentPower',
  'Current(Amps)': 'current',
  'Export Active Energy(kWh)': 'exportActiveEnergy',
  'Export Reactive Energy(kVArh)': 'exportReactiveEnergy',
  'Frequency(Hz)': 'frequency',
  'Import Active Energy(kWh)': 'importActiveEnergy',
  'Import Reactive Energy(kVArh)': 'importReactiveEnergy',
  'Phase Angle(Degrees)': 'phaseAngle',
  'Power Factor': 'powerFactor',
  'Reactive Power(VAr)': 'reactivePower',
  'Total Active Energy(kWh)': 'totalActiveEnergy',
  'Total Reactive Energy(kVArh)': 'totalReactiveEnergy',
  'Voltage(Volts)': 'voltage'
};

export const ELECTRICITY_FIELD = {
  activePower: {
    title: 'Active Power',
    unit: 'Watts'
  },
  apparentPower: {
    title: 'Apparent Power',
    unit: 'VoltAmps'
  },
  current: {
    title: 'Current',
    unit: 'Amps'
  },
  exportActiveEnergy: {
    title: 'Export Active Energy',
    unit: 'kWh'
  },
  exportReactiveEnergy: {
    title: 'Export Reactive Energy',
    unit: 'kVArh'
  },
  frequency: {
    title: 'Frequency',
    unit: 'Hz'
  },
  importActiveEnergy: {
    title: 'Import Active Energy',
    unit: 'kWh'
  },
  importReactiveEnergy: {
    title: 'Import Reactive Energy',
    unit: 'kVArh'
  },
  phaseAngle: {
    title: 'Phase Angle',
    unit: 'Degrees'
  },
  powerFactor: {
    title: 'Power Factor',
    unit: ''
  },
  reactivePower: {
    title: 'Reactive Power',
    unit: 'VAr'
  },
  totalActiveEnergy: {
    title: 'Total Active Energy',
    unit: 'kWh'
  },
  totalReactiveEnergy: {
    title: 'Total Reactive Energy',
    unit: 'kVArh'
  },
  voltage: {
    title: 'Voltage',
    unit: 'Volts'
  },
  time: {
    title: 'Time',
    unit: ''
  }
}

export const EMPTY_DATA: Electricity = {
  activePower: undefined,
  apparentPower: undefined,
  current: undefined,
  exportActiveEnergy: undefined,
  exportReactiveEnergy: undefined,
  frequency: undefined,
  importActiveEnergy: undefined,
  importReactiveEnergy: undefined,
  phaseAngle: undefined,
  powerFactor: undefined,
  reactivePower: undefined,
  totalActiveEnergy: undefined,
  totalReactiveEnergy: undefined,
  voltage: undefined,
  time: undefined
};

