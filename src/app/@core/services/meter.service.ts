import { Injectable } from "@angular/core";
import { AngularFireDatabase } from "@angular/fire/database";
import "firebase/database";
import { Observable } from "rxjs";
import {
  Electricity,
  ElectricityData,
  RawElectricity,
  EMPTY_DATA,
  MAP_DATA,
  ElectricityChart,
  ELECTRICITY_FIELD
} from "../data/electricity";
import * as moment from "moment";
import { map, switchMap } from 'rxjs/operators';

moment.locale("th");


@Injectable({
  providedIn: "root"
})
export class MeterService extends ElectricityData {
  private dataRef;

  private mapData = MAP_DATA; // use to rename object key to normal
  private emptyData = EMPTY_DATA;
  private electricityField: {[field: string]: { title: string, unit: string }} = ELECTRICITY_FIELD;

  private selectField: string[] = ["current", "powerFactor", "voltage", "activePower", "totalActiveEnergy"]

  constructor(db: AngularFireDatabase) {
    super();
    this.dataRef = db.list<RawElectricity>(`Node_A`, ref =>
      ref.orderByChild("time").limitToLast(90)
    );
  }

  getListData(): Observable<Electricity[]> {
    return this.getRawData().pipe(
      map(data =>{
        const change = data.map(obj => {
          let result = {};
          // map object key to regular name, convert string to number
          Object.keys(obj).forEach(key => {
            if (key != 'time') {
              result[this.mapData[key]] = Number(obj[key]);
            }
          });
          result["time"] = obj.time;
          return result as Electricity;
        })
        return change;
      })
    );
  }

  getChartData(): Observable<ElectricityChart> {
    return this.getListData().pipe(map(data => {
      let result: ElectricityChart = {};

      this.selectField.forEach(field => {
        result[field] = {
          title: this.electricityField[field].title,
          unit: this.electricityField[field].unit,
          time: data.map(o => moment.unix(o.time).format()),
          value: data.map(o => o[field])
        }
      })
      return result;
    }))
  }

  private getRawData(): Observable<RawElectricity[]> {
    return this.dataRef.valueChanges();
  }
}
